# Deploy a Polkadot node

## Description

This setup deploys a polkadot node together with monitoring in a hardened environment. 
The deployment is done in an automated way, but there is a one time manual setup as well.

## First steps

- Install azure-cli and run`az login` to authenticate with Azure
- Make infra-init.sh executable with `chmod +x infra-init.sh`, configure the variables in the script
  and run it

The script will automatically set up the Azure basic infrastructure requirements:
 - create a service principal for gitlab CI/CD to authenticate with Azure
 - create a resource group
 - create a storage account to store the Terraform state in

It will also automatically add the authentication vars to gitlab CI/CD if you configure the 
`GL_PROJECT_ID` and `GL_ACCESS_TOKEN` values in the script.

The rest will be handled automatically through the CI/CD pipeline. After doing these steps (and the SSH details steps below),
you can run the pipeline to deploy the node. This will automatically done as soon as you commit the code to git. If you
committed before doing these steps, just run the gitlab pipeline manually.

### Add the SSH details

Add the SSH details as CI/CD variables:
- SSH user as VM_ADMIN_USERNAME
- SSH private key as VM_ADMIN_PRIVATE_KEY
- SSH public key as VM_ADMIN_PUBLIC_KEY

### Optional: adding the Azure auth vars manually

The service principal authentication details will be returned after it has been created: 
```
{
  "appId": "xxxx-xxxx-xxxx",
  "displayName": "gitlab",
  "name": "xxxx-xxxx-xxxx",
  "password": "xxxx",
  "tenant": "xxxx-xxxx-xxxx"
}
```

They have to be stored as Gitlab CI/CD variables:
- appId as AZURE_SP_CLIENT_ID
- password as AZURE_SP_CLIENT_PW
- tenant as AZURE_SP_TENANT_ID

Also add the AZURE_SUB_ID var, that should contain the Azure subscription ID

### Optional: change the URLs

If you want to use different (Azure) URLs, you can change the PROJECT_URL_V4 and PROJECT_URL_V6 vars in .gitlab-ci.yml 

## CI/CD pipeline

These steps are part of and are performed automatically by the pipeline.

### build_image
Build a hardened ubuntu image.

**NOTE**: this job takes ~40 minutes to complete

_When_: at first run. This step won't replace the image if it already exists  
_When_: subsequently every time the ubuntu.pkr.hcl file has been modified.
This step will force replace the image

### validate
Validate the terraform code

_When_: every time a change was detected in the terraform code

### plan
Plan the terraform infrastructure change

_When_: every time a change was detected in the terraform code

### apply
Apply the infrastructure change planned in the previous step

_When_: every time a change was detected in the terraform code

### config
Apply further configuration via ansible

_When_: every time a change was detected in the polkadotnode ansible code