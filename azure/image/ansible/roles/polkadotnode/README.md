Role Name
=========

This role sets up a Polkadot node and does some extra hardening.

Feature examples:

- set up polkadot node, connected to kusama chain
- change default SSH port
- configure journald
- set up prometheus & alertmanager

Privileged permissions are required.

Role Variables
--------------

See defaults/main.yml

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
         - { role: roles/polkadotnode }
