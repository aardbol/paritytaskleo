variable "client_id" {
  default = env("ARM_CLIENT_ID")
}
variable "client_secret" {
  default = env("ARM_CLIENT_SECRET")
}
variable "tenant_id" {
  default = env("ARM_TENANT_ID")
}
variable "sub_id" {
  default = env("ARM_SUBSCRIPTION_ID")
}

variable "project_name" {
  default = env("PROJECT_NAME")
}

variable "project_path" {
  default = env("CI_PROJECT_DIR")
}

variable "project_location" {
  default = env("PROJECT_LOCATION")
}

source "azure-arm" "ubuntu20_04" {
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
  subscription_id = var.sub_id

  managed_image_name                = "ubuntu20_04-hardened"
  managed_image_resource_group_name = var.project_name
  location                          = var.project_location

  image_offer     = "0001-com-ubuntu-server-focal"
  image_sku       = "20_04-lts-gen2"
  image_publisher = "Canonical"
  os_type         = "Linux"
  vm_size         = "Standard_D4s_v5"
}

build {
  sources = ["sources.azure-arm.ubuntu20_04"]

  provisioner "ansible" {
    use_proxy        = false
    playbook_file    = "${var.project_path}/azure/image/ansible/hardenos.yml"
    extra_arguments  = ["-vv"]
  }
}