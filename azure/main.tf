data "azurerm_resource_group" "main" {
  name = var.project_name
}

resource "azurerm_virtual_network" "main" {
  name          = "main-vnet"
  address_space = ["10.0.0.0/16", "fd48:ed11:b92c:c548::/64"]
  dns_servers   = ["1.1.1.1", "1.0.0.1", "2606:4700:4700::1111", "2606:4700:4700::1001"]

  location            = var.location
  resource_group_name = data.azurerm_resource_group.main.name
}

resource "azurerm_subnet" "main" {
  name             = "main"
  address_prefixes = ["10.0.0.0/24", "fd48:ed11:b92c:c548::/64"]

  resource_group_name  = data.azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
}

resource "azurerm_ssh_public_key" "node_admin" {
  name       = var.node_admin
  public_key = var.node_admin_pubkey

  resource_group_name = data.azurerm_resource_group.main.name
  location            = var.location
}