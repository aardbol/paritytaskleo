output "node_ips" {
  value = azurerm_linux_virtual_machine.node.public_ip_addresses
}
