resource "azurerm_public_ip" "node-ipv4" {
  name                    = "node-ipv4"
  sku                     = "Standard"
  allocation_method       = "Static"
  ip_version              = "IPv4"
  idle_timeout_in_minutes = 30
  domain_name_label       = split(".", var.project_url_v4)[0]

  resource_group_name = data.azurerm_resource_group.main.name
  location            = var.location
}

resource "azurerm_public_ip" "node-ipv6" {
  name                    = "node-ipv6"
  sku                     = "Standard"
  allocation_method       = "Static"
  ip_version              = "IPv6"
  idle_timeout_in_minutes = 30
  domain_name_label       = split(".", var.project_url_v6)[0]

  resource_group_name = data.azurerm_resource_group.main.name
  location            = var.location
}

resource "azurerm_network_security_group" "node" {
  name                = "node-nsg"
  location            = var.location
  resource_group_name = data.azurerm_resource_group.main.name

  security_rule {
    name                       = "SSH"
    priority                   = 100
    source_address_prefix      = "*"
    source_port_range          = "*"
    destination_address_prefix = "*"
    destination_port_range     = "22"
    protocol                   = "Tcp"
    access                     = "Allow"
    direction                  = "Inbound"
  }

  security_rule {
    name                       = "DotNodeP2p"
    priority                   = 160
    protocol                   = "Tcp"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
    source_port_range          = "*"
    destination_port_range     = "30333"
    access                     = "Allow"
    direction                  = "Inbound"
  }

#  security_rule {
#    name                       = "DotNodeWebsocket"
#    priority                   = 162
#    protocol                   = "Tcp"
#    source_address_prefix      = "*"
#    destination_address_prefix = "*"
#    source_port_range          = "*"
#    destination_port_range     = "9944"
#    access                     = "Allow"
#    direction                  = "Inbound"
#  }
#
#  security_rule {
#    name                       = "DotNodeHttp"
#    priority                   = 163
#    protocol                   = "Tcp"
#    source_address_prefix      = "*"
#    destination_address_prefix = "*"
#    source_port_range          = "*"
#    destination_port_range     = "9933"
#    access                     = "Allow"
#    direction                  = "Inbound"
#  }

  security_rule {
    name                       = "Prometheus"
    priority                   = 180
    protocol                   = "Tcp"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
    source_port_range          = "*"
    destination_port_range     = "9090"
    access                     = "Allow"
    direction                  = "Inbound"
  }

#  security_rule {
#    name                       = "Alertmanager"
#    priority                   = 185
#    protocol                   = "Tcp"
#    source_address_prefix      = "*"
#    destination_address_prefix = "*"
#    source_port_range          = "*"
#    destination_port_range     = "9093"
#    access                     = "Allow"
#    direction                  = "Inbound"
#  }

#  security_rule {
#    name                       = "Web"
#    priority                   = 200
#    protocol                   = "Tcp"
#    source_address_prefix      = "*"
#    destination_address_prefix = "*"
#    source_port_range          = "*"
#    destination_port_ranges    = ["80", "443"]
#    access                     = "Allow"
#    direction                  = "Inbound"
#  }
}

resource "azurerm_subnet_network_security_group_association" "node-main" {
  subnet_id                 = azurerm_subnet.main.id
  network_security_group_id = azurerm_network_security_group.node.id
}

resource "azurerm_network_interface" "node" {
  name = "node-nic"

  ip_configuration {
    name                          = "ipconfig"
    primary                       = true
    subnet_id                     = azurerm_subnet.main.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.node-ipv4.id
  }

  ip_configuration {
    name                          = "ipconfig6"
    primary                       = false
    subnet_id                     = azurerm_subnet.main.id
    private_ip_address_allocation = "Dynamic"
    private_ip_address_version    = "IPv6"
    public_ip_address_id          = azurerm_public_ip.node-ipv6.id
  }

  resource_group_name = data.azurerm_resource_group.main.name
  location            = var.location
}

data "azurerm_image" "node" {
  name                = "ubuntu20_04-hardened"
  resource_group_name = var.project_name
}

resource "azurerm_linux_virtual_machine" "node" {
  name                  = "node-vm"
  size                  = var.node_size
  admin_username        = var.node_admin
  network_interface_ids = [azurerm_network_interface.node.id]
  source_image_id       = data.azurerm_image.node.id


  admin_ssh_key {
    username   = var.node_admin
    public_key = azurerm_ssh_public_key.node_admin.public_key
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    disk_size_gb         = var.disk_size
  }

  resource_group_name = data.azurerm_resource_group.main.name
  location            = var.location
}