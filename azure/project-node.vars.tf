variable "node_admin" {
  description = "Username of the VM admin"
}

variable "node_admin_privkey_path" {
  description = "Path to the SSH private key of the VM admin"
}

variable "node_admin_pubkey" {
  description = "SSH public key of the VM admin"
}

variable "node_size" {
  default = "Standard_D4s_v5"
}

variable "image" {
  type = map(string)
  default = {
    offer = "0001-com-ubuntu-server-focal"
    sku   = "20_04-lts-gen2"
  }
}

variable "disk_size" {
  type    = number
  default = 256
}