terraform {
  backend "azurerm" {
    storage_account_name = "paritytechtasktf"
    resource_group_name  = "paritytechtask"
    container_name       = "tfstate"
    key                  = "dev.terraform.tfstate"
  }
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  features {}
}
