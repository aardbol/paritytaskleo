variable "project_name" {
  description = "Name that will be used for e.g. the resource group"
}

variable "project_path" {
  description = "Project path in the Gitlab runner"
}

variable "project_url_v4" {
  description = "DNS subdomain for the public IPv4"

  validation {
    condition = can(regex("^.*\\.cloudapp.azure.com$", var.project_url_v4))
    error_message = "The project URL should be an Azure cloudapp domain: xxx.location.cloudapp.azure.com."
  }
}

variable "project_url_v6" {
  description = "DNS subdomain for the public IPv6"

  validation {
    condition = can(regex("^.*\\.cloudapp.azure.com$", var.project_url_v6))
    error_message = "The project URL should be an Azure cloudapp domain: xxx.location.cloudapp.azure.com."
  }
}

variable "sub_id" {
  description = "Azure subscription ID"

  validation {
    condition = can(regex("^[a-z0-9]+-[a-z0-9]+-[a-z0-9]+-[a-z0-9]+-[a-z0-9]+$", var.sub_id))
    error_message = "Invalid Azure subscription ID given."
  }
}

variable "location" {}
