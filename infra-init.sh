#!/usr/bin/env bash

# This script creates a service pincipal, resource group and storage account in Azure
# Requirements: azure-cli, curl
# Login with `az login` before running this script

#--- CONFIG: edit as needed

AZ_SUBSCRIPTION_ID=""
AZ_LOCATION="westeurope"
# If resource group name is changed here, be sure to update PROJECT_NAME
# in .gitlab-ci.yml and the terraform provider code
AZ_GROUP_NAME="paritytechtask"
# If storage account name is changed here, be sure to update the terraform provider code too
AZ_STORAGE_ACCOUNT_NAME="paritytechtasktf"

# If you leave the below vars empty, you'll have to add the CI/CD vars to Gitlab manually.
# See README.md for details

# Numeric ID value of the project
GL_PROJECT_ID=31907699
# Your own access token with API permission
GL_ACCESS_TOKEN=""

#---

GL_PROJECTS_URL="https://gitlab.com/api/v4/projects"

# Make sure the Gitlab CI/CD vars exist already, it will not overwrite existing vars
if [ -n "$GL_PROJECT_ID" ] && [ -n "$GL_ACCESS_TOKEN" ]; then
  curl --request POST --header "PRIVATE-TOKEN: $GL_ACCESS_TOKEN" "$GL_PROJECTS_URL/$GL_PROJECT_ID/variables" --form "key=AZURE_SP_CLIENT_ID" --form "value=nothing" &> /dev/null
  curl --request POST --header "PRIVATE-TOKEN: $GL_ACCESS_TOKEN" "$GL_PROJECTS_URL/$GL_PROJECT_ID/variables" --form "key=AZURE_SP_CLIENT_PW" --form "value=nothing" &> /dev/null
  curl --request POST --header "PRIVATE-TOKEN: $GL_ACCESS_TOKEN" "$GL_PROJECTS_URL/$GL_PROJECT_ID/variables" --form "key=AZURE_SP_TENANT_ID" --form "value=nothing" &> /dev/null
  curl --request POST --header "PRIVATE-TOKEN: $GL_ACCESS_TOKEN" "$GL_PROJECTS_URL/$GL_PROJECT_ID/variables" --form "key=AZURE_SUB_ID" --form "value=nothing" &> /dev/null
else
  printf "\nWARNING: Skipping creation of CI/CD vars because GL_ACCESS_TOKEN or GL_PROJECT_ID is empty\n"
fi

az account set -s "$AZ_SUBSCRIPTION_ID"

printf "\n--- Creating resource group\n"
az group create -n "$AZ_GROUP_NAME"

az config set defaults.location="$AZ_LOCATION" defaults.group="$AZ_GROUP_NAME"

printf "\n--- Creating service principal\n"
az ad sp create-for-rbac \
-n "gitlab" \
--role Contributor \
--scopes "/subscriptions/$AZ_SUBSCRIPTION_ID" > spdetails

printf "\n--- Updating Gitlab CI/CD vars\n"
if [ -n "$GL_PROJECT_ID" ] && [ -n "$GL_ACCESS_TOKEN" ]; then
  curl --request PUT --header "PRIVATE-TOKEN: $GL_ACCESS_TOKEN" "$GL_PROJECTS_URL/$GL_PROJECT_ID/variables/AZURE_SP_CLIENT_ID" --form "value=$(jq -r ".appId" < spdetails)"
  curl --request PUT --header "PRIVATE-TOKEN: $GL_ACCESS_TOKEN" "$GL_PROJECTS_URL/$GL_PROJECT_ID/variables/AZURE_SP_CLIENT_PW" --form "value=$(jq -r ".password" < spdetails)"
  curl --request PUT --header "PRIVATE-TOKEN: $GL_ACCESS_TOKEN" "$GL_PROJECTS_URL/$GL_PROJECT_ID/variables/AZURE_SP_TENANT_ID" --form "value=$(jq -r ".tenant" < spdetails)"
  curl --request PUT --header "PRIVATE-TOKEN: $GL_ACCESS_TOKEN" "$GL_PROJECTS_URL/$GL_PROJECT_ID/variables/AZURE_SUB_ID" --form "value=$AZ_SUBSCRIPTION_ID"
else
  printf "WARNING: Skipping updating of CI/CD vars because GL_ACCESS_TOKEN or GL_PROJECT_ID is empty\n"
fi

printf "\n--- Creating storage account\n"
az storage account create -n "$AZ_STORAGE_ACCOUNT_NAME" --sku Standard_ZRS --min-tls-version TLS1_2 --public-network-access Enabled

az storage account blob-service-properties update -n "$AZ_STORAGE_ACCOUNT_NAME" --enable-versioning true

az storage container create -n tfstate --account-name "$AZ_STORAGE_ACCOUNT_NAME" --public-access off

if [ -z "$GL_PROJECT_ID" ] || [ -z "$GL_ACCESS_TOKEN" ]; then
  printf "\n#######################################"
  printf "\nBe sure to add the CI/CD vars manually!"
  printf "\n#######################################"
  printf "\n\nDetails:"
  cat spdetails
fi

rm spdetails